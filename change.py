#!/usr/bin/env python3

import os

# Change the current working directory
print(os.getcwd())
os.chdir('/Users/patra/Desktop/python_practice')
print(os.getcwd())

#list directory contents
print(os.listdir())

# Create a directory
os.mkdir('new_dir')
print(os.listdir())

# Rename a directory
os.rename('new_dir', 'new_dir_renamed')
print(os.listdir())

# Remove a directory
os.rmdir('new_dir_renamed')
os.rmdir('new_dir2')
print(os.listdir())
