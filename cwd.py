#!/usr/bin/env python3

import os

# get the current working directory
cwd = os.getcwd() # os.path.abspath(cwd)
print(cwd)

# get the current working directory name
cwd_name = os.path.basename(cwd)
print(cwd_name)

# get the current working directory as a string
cwd_str = os.getcwdb()
print(cwd_str)

# get the current working directory parent directory
cwd_parent = os.path.dirname(cwd)
print(cwd_parent)

# get the user's home directory
home = os.path.expanduser('~') # os.getenv('HOME')
print(home)

# get the user's home directory name
home_name = os.path.basename(home)
print(home_name)
