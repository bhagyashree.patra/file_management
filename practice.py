#!/usr/bin/env python3

import os
import shutil

print(os.getcwd())
print(os.path.basename(os.getcwd()))

print(os.listdir())

# os.rename('new_dir', 'new_dir2')
# print(os.listdir())

os.chdir('/Users/patra/Desktop/python_practice/file_management/new_dir2')
print(os.getcwd())
print(os.listdir())

with open('file.txt', 'w') as f:
    f.write('Hello World')

print(os.listdir())

os.chdir('/Users/patra/Desktop/python_practice/file_management')
shutil.rmtree('new_dir2')
print(os.listdir())